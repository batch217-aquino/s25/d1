// console.log(65456465);

// Section Json Obj / JS Object Notation
// {
//     "city" : "Quezon City",
//     "province" : "Metro Manila",
//     "country" : "Philippines"
// }

// JSON Array

// ArrayName = cities
// "cities" : [
//     {"city" : "Quezon", 
//     "provine" : "Metro Manila", 
//     "country" : "Philippines"},

//     {"city" : "Manila City", 
//     "provine" : "Metro Manila", 
//     "country" : "Philippines"},

//     {"city" : "Makati City", 
//     "provine" : "Metro Manila", 
//     "country" : "Philippines"},
// ]

// [Section] JSON Methods

let batchesArr = [
    {
        batchName: "Batch217",
        schedule: "Full-Time"
    },
    {
        batchName: "Batch218",
        schedule: "Part-Time"
    }
]

//stringify --> converting method for JS obj into a string
console.log(`Result from stringify method:`);
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
    name: "John",
    age: 31,
    address: {
        city: "Manila",
        country: "Philippines"
    }
})
console.log(data);

// [Section] Using stringify method wwith variables
let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
    city: prompt("Which city do you live in?"),
    country: prompt("Which country does your city address belong to?"),
}
let otherData = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    age: age,
    address: address
})
console.log(otherData);

// [Secction] Converting stringify jason into js obj




// Parse Method

let batchesJSON = `[
    {
    "batchName" : "batch217",
    "schedule" : "Full-Time"
    },

    {"batchName" : "batch218",
    "schedule" : "Part-Time"}
]`
console.log("Result from parse metho");

console.log(JSON.parse(batchesJSON));



let stringifiedObj = `{
    "name" : "John",
    "age" : "32",
    "address" : {
        "city" : "Manila",
        "country" : "Philippines"
    }
}`
console.log(JSON.parse(stringifiedObj));